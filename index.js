(function () {
    var dialElement = document.getElementById('dial');
    var meterElement = document.getElementById('meter');
    setInterval(() => updateSpeedometerValue(dialElement, meterElement), 4000);
}());

function updateSpeedometerValue(dialElement, meterElement) {
    let deg = getRandomDeg();
    dialElement.style.transform = `rotate(${deg}deg)`;
    meterElement.style.backgroundColor = getColorByDeg(deg);
}

// 0 = -120deg; 60 = -40deg; 120 = 40deg; 180 = 120deg

function getRandomDeg() {
    return Math.random() * 240 - 120;
}

function getColorByDeg(deg) {
    return deg <= -40 ? 'rgba(0, 128, 0, 0.2)' : deg <= 40 ? 'rgba(255, 165, 0, 0.2)' : 'rgba(255, 0, 0, 0.2)';
}

